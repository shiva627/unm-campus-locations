package com.example.unmcampus;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class NetworkAwareActivity extends ActionBarActivity {

	private NetworkChangeReceiver networkChangeReceiver= new NetworkChangeReceiver();

	@Override
	protected void onStart() {		
		super.onStart();
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
		registerReceiver(networkChangeReceiver, filter);
	}

	@Override
	protected void onStop() {		
		super.onStop();
		unregisterReceiver(networkChangeReceiver);
	}

	private class NetworkChangeReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(final Context context, final Intent intent) {
			int status = NetworkUtil.getConnectivityStatus(context);
			if(status!=0) onNetworkConnected(status);
			else onNetworkDisconnected();
		}
	}

	/**
	 * This is the callback method that will be called when the device is connected to network. 
	 * @param type 1 for WIFI and 2 for MOBILE
	 */
	protected void onNetworkConnected(int type){}

	/**
	 * Callback method, when device is disconnected.
	 */
	protected void onNetworkDisconnected() {}

	protected static class NetworkUtil {

		public static int TYPE_WIFI = 1;
		public static int TYPE_MOBILE = 2;
		public static int TYPE_NOT_CONNECTED = 0;

		public static int getConnectivityStatus(Context context) {
			ConnectivityManager cm = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);

			NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
			if (null != activeNetwork) {
				if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
					return TYPE_WIFI;

				if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
					return TYPE_MOBILE;
			} 
			return TYPE_NOT_CONNECTED;
		}
	}
}

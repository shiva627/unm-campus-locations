package com.example.unmcampus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.skv.unmcampus.R;

public class CampusBuildingsActivity extends ActionBarActivity {

	ListAdapter ladapter;
	String clickedButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//		setContentView(R.layout.activity_campus_buildings);

		setContentView(R.layout.campuslayout);

		Button btnbuildings = (Button) findViewById(R.id.btnAbqBuilding);
		btnbuildings.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "ABQ Buildings", Toast.LENGTH_SHORT).show();
				clickedButton = "ABQ Buildings";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});


		Button btnBPN = (Button) findViewById(R.id.btnBPN);
		btnBPN.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "BluePhones North", Toast.LENGTH_SHORT).show();
				clickedButton = "BluePhones North";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});

		Button btnBPS = (Button) findViewById(R.id.btnBPS);
		btnBPS.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "BluePhones South", Toast.LENGTH_SHORT).show();
				clickedButton = "BluePhones South";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});		

		Button btnCP = (Button) findViewById(R.id.btnComputerPods);
		btnCP.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Computer Pods", Toast.LENGTH_SHORT).show();
				clickedButton = "Computer Pods";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});	

		Button btnDining = (Button) findViewById(R.id.btnDinings);
		btnDining.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Dinings", Toast.LENGTH_SHORT).show();
				clickedButton = "Dinings";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});	

		Button btnGyms = (Button) findViewById(R.id.btnGyms);
		btnGyms.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Gyms", Toast.LENGTH_SHORT).show();
				clickedButton = "Gyms";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});		

		Button btnvendiing = (Button) findViewById(R.id.btnVending);
		btnvendiing.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Healthy Vending", Toast.LENGTH_SHORT).show();
				clickedButton = "Healthy Vending";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});		


		Button btnic = (Button) findViewById(R.id.btnIC);
		btnic.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Information Centers", Toast.LENGTH_SHORT).show();
				clickedButton = "Information Centers";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});		

		Button btnls = (Button) findViewById(R.id.btnLS);
		btnls.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Location Stations", Toast.LENGTH_SHORT).show();
				clickedButton = "Location Stations";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});			


		Button btnlib = (Button) findViewById(R.id.btnLib);
		btnlib.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Libraries", Toast.LENGTH_SHORT).show();
				clickedButton = "Libraries";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});		

		Button btnmp = (Button) findViewById(R.id.btnMP);
		btnmp.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Metered Parking", Toast.LENGTH_SHORT).show();
				clickedButton = "Metered Parking";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});

		Button btnoc = (Button) findViewById(R.id.btnOC);
		btnoc.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Other Campuses", Toast.LENGTH_SHORT).show();
				clickedButton = "Other Campuses";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});	

		Button btnpi = (Button) findViewById(R.id.btnPI);
		btnpi.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Places of Interest", Toast.LENGTH_SHORT).show();
				clickedButton = "Places of Interest";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});		

		Button btnrr = (Button) findViewById(R.id.btnRestrooms);
		btnrr.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Restrooms", Toast.LENGTH_SHORT).show();
				clickedButton = "Restrooms";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});	

		Button btnus = (Button) findViewById(R.id.btnUS);
		btnus.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "UNM Shuttles", Toast.LENGTH_SHORT).show();
				clickedButton = "UNM Shuttles";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});	

		Button btnwr = (Button) findViewById(R.id.btnWR);
		btnwr.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Walking Routes", Toast.LENGTH_SHORT).show();
				clickedButton = "Walking Routes";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});	
		
		
		Button btnHSC = (Button) findViewById(R.id.btnHSC);
		btnHSC.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "HSC", Toast.LENGTH_SHORT).show();
				clickedButton = "HSC";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});	
		
		Button btnUnivRR = (Button) findViewById(R.id.btnUnivRR);
		btnUnivRR.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Universal Restrooms", Toast.LENGTH_SHORT).show();
				clickedButton = "Universal Restrooms";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});	
		
		Button btnTimeclock = (Button) findViewById(R.id.btnTimeclock);
		btnTimeclock.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Time Clock", Toast.LENGTH_SHORT).show();
				clickedButton = "Time Clock";

				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
				intent.putExtra("ClickedBuildingGroup", clickedButton);
				startActivity(intent);
			}
		});	


		//		final ListView listview = (ListView) findViewById(R.id.listViewBuildings);
		//
		//		String[] buildingValues = new String[] { "ABQ Buildings", "BluePhones North",
		//				"BluePhones South", "Computer Pods", "Dinings", "Gyms", "Healthy Vending",
		//				"Information Centers", "Location Stations", "Libraries", "Location Index", 
		//				"Metered Parking", "Other Campuses", "Places of Interest", "Restrooms", 
		//				"UNM Shuttles", "Walking Routes"};
		//
		//		final ArrayList<String> listBuildings = new ArrayList<String>();
		//		for(int i=0;i<buildingValues.length;i++){
		//			listBuildings.add(buildingValues[i]);
		//		}
		//
		//		final StableArrayAdapter adapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, listBuildings);
		//		listview.setAdapter(adapter);
		//		listview.setClickable(true);
		//
		//		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
		//
		//			@Override
		//			public void onItemClick(AdapterView<?> parent, View view,
		//					int position, long id) {
		//
		//				Object listObj = parent.getItemAtPosition(position);
		//				String clickedBuildingGroup = listObj.toString();
		//				Toast.makeText(getApplicationContext(), clickedBuildingGroup,Toast.LENGTH_SHORT).show();
		//
		//				Intent intent = new Intent(getApplicationContext(),SpecificBuildingsActivity.class);
		//				intent.putExtra("ClickedBuildingGroup", clickedBuildingGroup);
		//				startActivity(intent);
		//			}
		//		});
	}

	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.campus_buildings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void ButtonOnClick(View v) {
		switch (v.getId()) {
		case R.id.btnAbqBuilding:
			System.out.print("vgvgvghvgv");
			break;
		}
	}
}

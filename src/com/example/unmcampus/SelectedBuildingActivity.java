package com.example.unmcampus;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;

import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.skv.unmcampus.R;

public class SelectedBuildingActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_selected_building);

		//		Bundle b = getIntent().getExtras();
		//		
		//		ArrayList<String> building = b.getStringArrayList("selectedbuilding");
		//		for(int i=0;i<building.size();i++){
		//			System.out.println(building.get(i));
		//		}

		Intent in = getIntent();
		final HashMap<String, String> hmapBuiilding = (HashMap<String, String>) in.getSerializableExtra("selectedbuilding");
		
		setTitle(hmapBuiilding.get("title"));
		
		System.out.println("title -- "+hmapBuiilding.get("title"));
		System.out.println("buildingnum -- "+hmapBuiilding.get("buildingnum"));
		System.out.println("abbr -- "+hmapBuiilding.get("abbr"));
		System.out.println("campus -- "+hmapBuiilding.get("campus"));
		System.out.println("keywords -- "+hmapBuiilding.get("keywords"));
		System.out.println("longitude -- "+hmapBuiilding.get("longitude"));
		System.out.println("latitude -- "+hmapBuiilding.get("latitude"));
		System.out.println("image -- "+hmapBuiilding.get("image"));
		System.out.println("link -- "+hmapBuiilding.get("link"));
		System.out.println("id -- "+hmapBuiilding.get("id"));
		System.out.println("description -- "+hmapBuiilding.get("description"));

		TextView tvTitle = (TextView) findViewById(R.id.textViewRTitle);
		TextView tvBuildingNum = (TextView) findViewById(R.id.textViewRBuildingNumber);
		TextView tvAbbr = (TextView) findViewById(R.id.textViewRAbbrevation);
		TextView tvCampus = (TextView) findViewById(R.id.textViewRCampus);
		TextView tvKeywords = (TextView) findViewById(R.id.textViewRKeywords);
		TextView tvLongitude = (TextView) findViewById(R.id.textViewRLongitude);
		TextView tvLatitude = (TextView) findViewById(R.id.textViewRLatitude);
		TextView tvImage = (TextView) findViewById(R.id.textViewRImage);
		TextView tvLink = (TextView) findViewById(R.id.textViewRLink);
		TextView tvID = (TextView) findViewById(R.id.textViewRID);
		EditText tvDescription = (EditText) findViewById(R.id.textViewRDescription);
		Button btnViewMap = (Button) findViewById(R.id.buttonViewMap);
		
		btnViewMap.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getApplicationContext(),GoogleMapActivity.class);
				intent.putExtra("Latitude", hmapBuiilding.get("latitude"));
				intent.putExtra("Longitude", hmapBuiilding.get("longitude"));
				intent.putExtra("Title", hmapBuiilding.get("title"));
				startActivity(intent);
			}
		});

		tvTitle.setMovementMethod(new ScrollingMovementMethod());
		tvBuildingNum.setMovementMethod(new ScrollingMovementMethod());
		tvAbbr.setMovementMethod(new ScrollingMovementMethod());
		tvCampus.setMovementMethod(new ScrollingMovementMethod());
		tvKeywords.setMovementMethod(new ScrollingMovementMethod());
		tvLongitude.setMovementMethod(new ScrollingMovementMethod());
		tvLatitude.setMovementMethod(new ScrollingMovementMethod());
		tvImage.setMovementMethod(new ScrollingMovementMethod());
		tvLink.setMovementMethod(new ScrollingMovementMethod());
		tvID.setMovementMethod(new ScrollingMovementMethod());
		
		
		tvDescription.setBackgroundColor(Color.TRANSPARENT);
		tvDescription.setTextColor(Color.GRAY);
		tvDescription.setTextSize(14);
		tvDescription.setTypeface(null, Typeface.NORMAL);
		
		String longitude = hmapBuiilding.get("longitude");
		double dlong = round(Double.parseDouble(longitude),3);
		
		String latitude = hmapBuiilding.get("latitude");
		double dlat = round(Double.parseDouble(latitude),3);
		
		tvTitle.setText(hmapBuiilding.get("title"));
		tvBuildingNum.setText(hmapBuiilding.get("buildingnum"));
		tvAbbr.setText(hmapBuiilding.get("abbr"));
		tvCampus.setText(hmapBuiilding.get("campus"));
		tvKeywords.setText(hmapBuiilding.get("keywords"));
		tvLongitude.setText(String.valueOf(dlong));
		tvLatitude.setText(String.valueOf(dlat));
		tvImage.setText(hmapBuiilding.get("image"));
		tvLink.setText(hmapBuiilding.get("link"));
		tvID.setText(hmapBuiilding.get("id"));
		tvDescription.setText(Html.fromHtml(hmapBuiilding.get("description")));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.selected_building, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}

package com.example.unmcampus;


import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.skv.unmcampus.R;

import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class GoogleMapActivity extends FragmentActivity implements OnMapClickListener{

	private GoogleMap gMap;
	String Latitude;
	String Longitude;
	String PLACE_NAME;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_google_map);

		Bundle extras = getIntent().getExtras();
		if(extras!=null){
			Latitude = extras.getString("Latitude");
			Longitude = extras.getString("Longitude");
			PLACE_NAME = extras.getString("Title");
		}

		System.out.println(Latitude);
		System.out.println(Longitude);
		Toast.makeText(getApplicationContext(), Latitude,Toast.LENGTH_SHORT).show();
		Toast.makeText(getApplicationContext(), Longitude,Toast.LENGTH_SHORT).show();
		SupportMapFragment mapFragment =
				(SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		//        mapFragment.getMapAsync(this);
		//		gMap = mapFragment.getMap();

		PackageManager mngr = getPackageManager();
		try {
			PackageInfo pkInfo = mngr.getPackageInfo("com.google.android.apps.maps", 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String geoCode = "geo:0,0?q=" + Latitude + ","
				+ Longitude + "(" + PLACE_NAME + ")";
		Intent sendLocationToMap = new Intent(Intent.ACTION_VIEW,
				Uri.parse(geoCode));
		startActivity(sendLocationToMap);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.google_map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onMapClick(LatLng arg0) {
		// TODO Auto-generated method stub

	}
}

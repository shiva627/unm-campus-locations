package com.example.unmcampus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ParseException;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.skv.unmcampus.R;

public class SpecificBuildingsActivity extends ActionBarActivity {

	public String URLBuildings = "http://datastore.unm.edu/locations/";
	public String buildingGroup;
	public String finalURL;
	private ProgressDialog progressDialog;
	public static final String TAG_TITLE = "title";
	public static final String TAG_BUILDINGNUM = "buildingnum";
	public static final String TAG_ABBR = "abbr";
	public static final String TAG_CAMPUS = "campus";
	public static final String TAG_KEYWORDS = "keywords";
	public static final String TAG_Longitude = "longitude";
	public static final String TAG_Latitude = "latitude";
	public static final String TAG_IMAGE = "image";
	public static final String TAG_LINK = "link";
	public static final String TAG_ID = "id";
	public static final String TAG_DESCRIPTION = "description";
	ArrayList<String> specficbuilding = new ArrayList<String>();
	HashMap<String, ArrayList<String>> hmap1 = new HashMap<String, ArrayList<String>>();
	HashMap<String, HashMap<String, String>> hmap = new HashMap<String, HashMap<String,String>>();
	ListAdapter ladapter;
	StableArrayAdapter adapter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_specific_buildings);

		final ListView listview = (ListView) findViewById(R.id.listViewSpecificBuildings);
		listview.setBackgroundColor(Color.BLACK);
		final EditText inputSearch = (EditText) findViewById(R.id.inputSearch);

		Intent intent = getIntent();
		String receiveClickedBuildingGroup = intent.getStringExtra("ClickedBuildingGroup");
		setTitle(receiveClickedBuildingGroup);				// setting title for activity
		//		Log.d("chkrcvitem",receiveClickedBuildingGroup);

		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage("Fetching details..");

		if(receiveClickedBuildingGroup.contains("ABQ Buildings")){
			buildingGroup = "abqbuildings.json";
		}
		else if (receiveClickedBuildingGroup.contains("BluePhones North")) {
			buildingGroup = "bluephonesnorth.json";
		}
		else if (receiveClickedBuildingGroup.contains("BluePhones South")) {
			buildingGroup = "bluephonessouth.json";
		}
		else if (receiveClickedBuildingGroup.contains("Computer Pods")) {
			buildingGroup = "computerpods.json";
		}
		else if (receiveClickedBuildingGroup.contains("Dinings")) {
			buildingGroup = "dining.json";
		}
		else if (receiveClickedBuildingGroup.contains("Gyms")) {
			buildingGroup = "gyms.json";
		}
		else if (receiveClickedBuildingGroup.contains("Healthy Vending")) {
			buildingGroup = "healthyvending.json";
		}
		else if (receiveClickedBuildingGroup.contains("Information Centers")) {
			buildingGroup = "infocenters.json";
		}
		else if (receiveClickedBuildingGroup.contains("Location Stations")) {
			buildingGroup = "lactationstations.json";
		}
		else if (receiveClickedBuildingGroup.contains("Libraries")) {
			buildingGroup = "libraries.json";
		}
		else if (receiveClickedBuildingGroup.contains("Location Index")) {
			buildingGroup = "locationindex.json";
		}
		else if (receiveClickedBuildingGroup.contains("Metered Parking")) {
			buildingGroup = "meteredparking.json";
		}
		else if (receiveClickedBuildingGroup.contains("Other Campuses")) {
			buildingGroup = "othercampuses.json";
		}
		else if (receiveClickedBuildingGroup.contains("Places of Interest")) {
			buildingGroup = "placesofinterest.json";
		}
		else if (receiveClickedBuildingGroup.contains("Restrooms")) {
			buildingGroup = "restrooms.json";
		}
		else if (receiveClickedBuildingGroup.contains("UNM Shuttles")) {
			buildingGroup = "unmshuttles.json";
		}
		else if(receiveClickedBuildingGroup.contains("Walking Routes")) {
			buildingGroup = "walkingroutes.json";
		}
		else if(receiveClickedBuildingGroup.contains("HSC")) {
			buildingGroup = "hsc.json";
		}
		else if(receiveClickedBuildingGroup.contains("Universal Restrooms")) {
			buildingGroup = "universalrestrooms.json";
		}
		else if(receiveClickedBuildingGroup.contains("Time Clock")) {
			buildingGroup = "timeclocks.json";
		}

		finalURL = URLBuildings+buildingGroup;
		Log.d("finalurl",finalURL);


		progressDialog.show();

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try
				{
					JSONArray jsonArray = getJSONfromURL(finalURL);
					// TODO
					for(int i=0;i<jsonArray.length();i++){
						JSONObject c = jsonArray.getJSONObject(i);
						String title = c.getString(TAG_TITLE);

						specficbuilding.add(title);

						String buildingnum = c.getString(TAG_BUILDINGNUM);
						String abbr = c.getString(TAG_ABBR);
						String campus = c.getString(TAG_CAMPUS);
						String keywords = c.getString(TAG_KEYWORDS);
						String longitude = c.getString(TAG_Longitude);
						String latitude = c.getString(TAG_Latitude);
						String image = c.getString(TAG_IMAGE);
						String link = c.getString(TAG_LINK);
						String id = c.getString(TAG_ID);
						String description = c.getString(TAG_DESCRIPTION);

						//						ArrayList<String> buildingDetails = new ArrayList<String>();
						//						buildingDetails.add(title);
						//						buildingDetails.add(buildingnum);
						//						buildingDetails.add(abbr);
						//						buildingDetails.add(campus);
						//						buildingDetails.add(keywords);
						//						buildingDetails.add(longitude);
						//						buildingDetails.add(latitude);
						//						buildingDetails.add(image);
						//						buildingDetails.add(link);
						//						buildingDetails.add(id);
						//						buildingDetails.add(description);

						HashMap<String, String> hmapBuildingDetails = new HashMap<String, String>();

						if(TextUtils.isEmpty(title)){
							hmapBuildingDetails.put("title","");
						}else{
							hmapBuildingDetails.put("title",title);
						}

						if(TextUtils.isEmpty(buildingnum)){
							hmapBuildingDetails.put("buildingnum","");
						}else{
							hmapBuildingDetails.put("buildingnum",buildingnum);
						}

						if(TextUtils.isEmpty(abbr)){
							hmapBuildingDetails.put("abbr","");
						}else{
							hmapBuildingDetails.put("abbr",abbr);
						}

						if(TextUtils.isEmpty(campus)){
							hmapBuildingDetails.put("campus","");
						}else{
							hmapBuildingDetails.put("campus",campus);
						}

						if(TextUtils.isEmpty(keywords)){
							hmapBuildingDetails.put("keywords","");
						}else{
							hmapBuildingDetails.put("keywords",keywords);
						}

						if(TextUtils.isEmpty(longitude)){
							hmapBuildingDetails.put("longitude","");
						}else{
							hmapBuildingDetails.put("longitude",longitude);
						}

						if(TextUtils.isEmpty(latitude)){
							hmapBuildingDetails.put("latitude","");
						}else{
							hmapBuildingDetails.put("latitude",latitude);
						}

						if(TextUtils.isEmpty(image)){
							hmapBuildingDetails.put("image","");
						}else{
							hmapBuildingDetails.put("image",image);
						}

						if(TextUtils.isEmpty(link)){
							hmapBuildingDetails.put("link","");
						}else{
							hmapBuildingDetails.put("link",link);
						}

						if(TextUtils.isEmpty(id)){
							hmapBuildingDetails.put("id","");
						}else{
							hmapBuildingDetails.put("id",id);
						}

						if(TextUtils.isEmpty(description)){
							hmapBuildingDetails.put("description","");
						}else{
							hmapBuildingDetails.put("description",description);
						}

						//						hmap1.put(title, buildingDetails);
						hmap.put(title, hmapBuildingDetails);
					}


				}catch(Exception e){
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if(progressDialog.isShowing())
							progressDialog.dismiss();

						adapter = new StableArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, specficbuilding);
						listview.setAdapter(adapter);

						inputSearch.addTextChangedListener(new TextWatcher() {

							@Override
							public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
								// When user changed the Text
								SpecificBuildingsActivity.this.adapter.getFilter().filter(cs);   
							}
							@Override
							public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
									int arg3) {
								// TODO Auto-generated method stub
							}
							@Override
							public void afterTextChanged(Editable arg0) {
								// TODO Auto-generated method stub                          
							}
						});

						listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {

								Object listObj = parent.getItemAtPosition(position);
								String clickedBuilding = listObj.toString();
								Toast.makeText(getApplicationContext(), clickedBuilding,Toast.LENGTH_SHORT).show();

								ArrayList<String> clickedBuildingDetails = hmap1.get(clickedBuilding);
								HashMap<String, String> hmapClickedBuildingDetails = hmap.get(clickedBuilding);
								Bundle b = new Bundle();
								//								b.putSerializable("selectedbuilding", clickedBuildingDetails);
								b.putSerializable("selectedbuilding", hmapClickedBuildingDetails);
								Intent i = new Intent(getApplicationContext(),SelectedBuildingActivity.class);
								i.putExtras(b);
								startActivity(i);
							}
						});
					}
				});
			}
		});
		thread.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.specific_buildings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static JSONArray getJSONfromURL(String url){

		InputStream is = null;
		String result = "";
		JSONObject jsonObj = null;
		JSONArray jsonArray = null;
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httppost = new HttpGet(url);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}catch(Exception e){
			Log.e("log_tag", "Error in http connection "+e.toString());
		}
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result=sb.toString();
			//			Log.d("check json", ""+result);
		}catch(Exception e){
			Log.e("log_tag", "Error converting result "+e.toString());
		}
		try{
			//			Log.d("check json array", ""+result);
			//			jsonObj = new JSONObject(result);
			jsonArray = new JSONArray(result);
		}catch(JSONException e){
			Log.e("log_tag", "Error parsing data "+e.toString());
		}
		//Log.d("check json obj", ""+jsonObj);
		//		return jsonObj;
		return jsonArray;
	}


	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

	}
}
